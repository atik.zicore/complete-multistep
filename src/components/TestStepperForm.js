import React, { useState } from "react";

const TestStepperForm = () => {
  const [page, setPage] = useState(0);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [mobile, setMobile] = useState("");
  const [address, setAddress] = useState("");
  const [district, setDistrict] = useState("");
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPass, setConfirmPass] = useState("");

  const handleFormSubmit = (e) => {
    e.preventDefault();

    console.log("Form Data:",{
      name,
      email,
      mobile,
      address,
      district,
      userName,
      password,
      confirmPass,
  });
  };

  const FormTitles = ["Personal", "Other", "sign up"];

  return (
    <div className="h-screen w-full bg-[#4b4b4b] text-center">
      <form className="text-white py-4 space-y-4" onSubmit={handleFormSubmit}>
        <h2>{FormTitles[page]} Information</h2>
        {/* Personal Info */}
        <div className={page === 0 ? "block" : "hidden"}>
          <div className="flex gap-4 justify-center items-center">
            <label htmlFor="name" className="capitalize text-white">
              Name
            </label>
            <input
              type="text"
              placeholder="Enter Name"
              name="Name"
              id="name"
              className="outline-none border border-black/40 p-2 bg-transparent"
              onChange={(e) => setName(e.target.value)}
              value={name}
            />
          </div>
          <div className="flex gap-4 justify-center items-center">
            <label htmlFor="email" className="capitalize text-white">
              Email
            </label>
            <input
              type="email"
              placeholder="Enter Email"
              name="email"
              id="email"
              className="outline-none border border-black/40 p-2 bg-transparent"
              onChange={(e) => setEmail(e.target.value)}
              value={email}
            />
          </div>
          <div className="flex gap-4 justify-center items-center">
            <label htmlFor="mobile" className="capitalize text-white">
              Mobile
            </label>
            <input
              type="tel"
              maxLength={11}
              placeholder="Enter Mobile Number"
              name="mobile"
              id="mobile"
              className="outline-none border border-black/40 p-2 bg-transparent"
              onChange={(e) => setMobile(e.target.value)}
              value={mobile}
            />
          </div>
        </div>
        {/* Other Info */}
        <div className={page === 1 ? "block" : "hidden"}>
          <div className="flex gap-4 justify-center items-center">
            <label htmlFor="address" className="capitalize text-white">
              Address
            </label>
            <input
              type="text"
              placeholder="Enter address"
              name="address"
              id="address"
              className="outline-none border border-black/40 p-2 bg-transparent"
              onChange={(e) => setAddress(e.target.value)}
              value={address}
            />
          </div>
          <div className="flex gap-4 justify-center items-center">
            <label htmlFor="district" className="capitalize text-white">
              District
            </label>
            <input
              type="text"
              placeholder="Enter district"
              name="district"
              id="district"
              className="outline-none border border-black/40 p-2 bg-transparent"
              onChange={(e) => setDistrict(e.target.value)}
              value={district}
            />
          </div>
        </div>
        {/* Signup Info */}
        <div className={page === 2 ? "block" : "hidden"}>
          <div>
            <label htmlFor="userName">User Name</label>
            <input
              type="text"
              placeholder="User Name"
              className="p-2 border border-black/20 bg-transparent"
              onChange={(e) => setUserName(e.target.value)}
              value={userName}
            />
          </div>
          <div>
            <label htmlFor="password">Password</label>
            <input
              type="text"
              placeholder="User password"
              className="p-2 border border-black/20 bg-transparent"
              onChange={(e) => setPassword(e.target.value)}
              value={password}
            />
          </div>
          <div>
            <label htmlFor="userName">Confirm Password</label>
            <input
              type="text"
              placeholder="confirm password"
              className="p-2 border border-black/20 bg-transparent"
              onChange={(e) => setConfirmPass(e.target.value)}
              value={confirmPass}
            />
          </div>
        </div>

        <div
        //   className={
        //     (( name && email && mobile )|| (address && district )? "block" : "hidden") +
        //     " flex gap-4 justify-center"
        //   } 

        className={(page === 0 && name && email && mobile ? "block" : page === 1 && address && district ? "block" : page === 2 && userName && password && confirmPass ? "block" : "hidden")}
        >
         
          {page === FormTitles.length - 1 ? (
            <input type="submit" className="bg-emerald-300" />
          ) : (
            <button
              type="button"
              className="px-4 py-2 border border-black capitalize "
              onClick={() => setPage(page + 1)}
            >
              next
            </button>
          )}
          {/* <button type="button" className="px-4 py-2 border border-black capitalize " onClick={() => setPage(page + 1)}>next</button> */}
        </div>
        <button
            type="button"
            className={
              (page === 0 ? "bg-gray-500 hidden" : " bg-blue-500") +
              " border border-black capitalize px-4 py-2"
            }
            onClick={() => setPage(page - 1)}
          >
            prev
          </button>

        {/* <div className={page === FormTitles.length -1 ? "block" : "hidden"}>
          <button
            type="submit"
            className="bg-emerald-400 px-4 py-2 cursor-pointer"
          >
            Submit
          </button>
        </div> */}
      </form>
    </div>
  );
};

export default TestStepperForm;
