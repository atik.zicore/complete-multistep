import React, { useState } from "react";

const PersonalInfo = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [mobile, setMobile] = useState("");

  return (
    <div className="w-full h-full">
      <div className="flex gap-4 justify-center items-center">
        <label htmlFor="name" className="capitalize text-white">
          Name
        </label>
        <input
          type="text"
          placeholder="Enter Name"
          name="Name"
          id="name"
          className="outline-none border border-black/40 p-2 bg-transparent"
          onChange={(e) => setName(e.target.value)}
          value={name}
        />
      </div>
      <div className="flex gap-4 justify-center items-center">
        <label htmlFor="email" className="capitalize text-white">
          Email
        </label>
        <input
          type="email"
          placeholder="Enter Email"
          name="email"
          id="email"
          className="outline-none border border-black/40 p-2 bg-transparent"
          onChange={(e) => setEmail(e.target.value)}
          value={email}
        />
      </div>
      <div className="flex gap-4 justify-center items-center">
        <label htmlFor="mobile" className="capitalize text-white">
          Mobile
        </label>
        <input
          type="tel"
          maxLength={11}
          placeholder="Enter Mobile Number"
          name="mobile"
          id="mobile"
          className="outline-none border border-black/40 p-2 bg-transparent"
          onChange={(e) => setMobile(e.target.value)}
          value={mobile}
        />
      </div>
    </div>
  );
};

export default PersonalInfo;
