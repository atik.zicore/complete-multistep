import React from "react";

const Signup = ({ formData, setFormData }) => {
  return (
    <section className="w-full h-full">
      <form className="w-full h-full">
        <div className="">
          <label className="block">user name</label>
          <input
            className=""
            required
            type="text"
            placeholder="user Name"
            value={formData.userName}
            onChange={(e) =>
              setFormData({ ...formData, userName: e.target.value })
            }
          />
        </div>
        <div>
          <label className="block">email</label>
          <input
            className=""
            required
            type="text"
            placeholder="email..."
            value={formData.email}
            onChange={(e) =>
              setFormData({ ...formData, email: e.target.value })
            }
          />
        </div>
        <div>
          <label className="block">password</label>
          <input
            type="text"
            required
            placeholder="password"
            value={formData.password}
            onChange={(e) =>
              setFormData({ ...formData, password: e.target.value })
            }
          />
        </div>
      </form>
    </section>
  );
};

export default Signup;
