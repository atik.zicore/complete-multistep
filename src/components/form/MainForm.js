import React, { useState } from "react";
import Signup from "./Signup";
import PersonalInfo from "./PersonalInfo";
import OtherInfo from "./OtherInfo";

const MainForm = () => {
  const [page, setPage] = useState(0);

  const FormTitles = ["Sign up info", "Personal Info", "other info"];
  const [formData, setFormData] = useState({
    userName: "",
    email: "",
    password: "",
    firstName: "",
    lastName: "",
  });
  const PageDisplay = () => {
    if (page === 0) {
      return <Signup formData={formData} setFormData={setFormData} />;
    } else if (page === 1) {
      return <PersonalInfo formData={formData} setFormData={setFormData} />;
    } else {
      return <OtherInfo />;
    }
  };

  return (
    <section className="w-full h-screen bg-[#636e72]  pt-20">
      <div className="text-center border border-white/50 w-1/3 mx-auto py-6">
        <h1 className="capitalize text-lg xl:text-2xl text-white font-bold">
          fill the input with necessary data
        </h1>

        <div className="form-container space-y-4">
          <div className="progressbar w-full h-4 ">
            <div
              style={{
                width: page === 0 ? "33.3%" : page === 1 ? "66.6%" : "100%",
              }}
            ></div>
          </div>

          <div className="header py-6 bg-emerald-400">
            <h2 className="text-white font-extrabold capitalize text-lg lg:text-xl">
              {FormTitles[page]}
            </h2>
          </div>
          <div className="body">{PageDisplay()}</div>
          <div className="footer flex gap-4 lg:gap-6  justify-center pt-4">
            <button
              className="px-4 py-2 rounded-xl capitalize border border-white outline-none w-1/4"
              onClick={() => setPage((currentPage) => currentPage - 1)}
            >
              Prev
            </button>
            <button
              className="px-4 py-2 rounded-xl capitalize border border-white outline-none w-1/4"
              onClick={() => {
                if (page === FormTitles.length - 1) {
                  alert(JSON.stringify(formData));
                  console.log(formData);
                } else {
                  setPage((currentPage) => currentPage + 1);
                }
              }}
            >
              {page === FormTitles.length - 1 ? "submit" : "next"}
            </button>
          </div>
        </div>
      </div>
    </section>
  );
};

export default MainForm;
