import React from "react";

const PersonalInfo = ({ formData, setFormData }) => {
  return (
    <div>
      <section className="w-full h-full">
        <form className="w-full h-full">
          <div className="">
            <label className="block">First Name</label>
            <input
              className=""
              type="text"
              placeholder="fiurst Name"
              value={formData.firstName}
              onChange={(e) =>
                setFormData({ ...formData, firstName: e.target.value })
              }
            />
          </div>
          <div>
            <label className="block">last Name</label>
            <input
              className=""
              type="text"
              placeholder="email..."
              value={formData.lastName}
              onChange={(e) =>
                setFormData({ ...formData, lastName: e.target.value })
              }
            />
          </div>
        </form>
      </section>
    </div>
  );
};

export default PersonalInfo;
