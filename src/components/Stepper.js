import React, { useState } from "react";

const Stepper = () => {
  const [step1, setStep1] = useState(false);
  const [step2, setStep2] = useState(false);
  const [step3, setStep3] = useState(false);
  const [value1, setValue1] = useState("");
  const [value2, setValue2] = useState("");
  const [value3, setValue3] = useState("");
  const [value4, setValue4] = useState("");
  const [value5, setValue5] = useState("");

  console.log(value1);
  console.log(value2);
  console.log(value3);
  console.log(value4);
  console.log(value5);

  const handleSubmit = (e) => {
    // Prevent the default form submission behavior
    e.preventDefault();

    // Update state variables with input values
    setValue1(e.target.elements[0].value);
    setValue2(e.target.elements[1].value);
    setValue3(e.target.elements[2].value);
    setValue4(e.target.elements[3].value);
    setValue5(e.target.elements[4].value);
  };


  
  return (
    <form
      onSubmit={(e) => handleSubmit(e)}
      className="flex justify-start flex-col items-center space-y-2"
    >
      <div>
        <input
          name="email"
          type="text"
          className="border border-gray-500 outline-none rounded-md"
        />
        <input
          name="password"
          type="text"
          className="border border-gray-500 outline-none rounded-md"
        />
      </div>
      <div className={value1 && value2 ? "block" : "hidden"}>
        <input
          name="phone"
          type="text"
          className="border border-gray-500 outline-none rounded-md"
        />
      </div>
      <div className={value3 && value2 && value1 ? "block" : "hidden"}>
        <input
          name="address"
          type="text"
          className="border border-gray-500 outline-none rounded-md"
        />
        <input
          name="father"
          type="text"
          className="border border-gray-500 outline-none rounded-md"
        />
      </div>
      
      <input type="submit" value="submit" />
    </form>
  );
};

export default Stepper;
