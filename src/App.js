import Stepper from "./components/Stepper";
import TestStepperForm from "./components/TestStepperForm";
import "./index.css";

function App() {
  return (
    <div className="App">
      {/* <MainForm/> */}
      {/* <Stepper /> */}
      <TestStepperForm/>
    </div>
  );
}

export default App;
